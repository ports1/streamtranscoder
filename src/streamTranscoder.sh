#!/bin/sh
#
# This script will run ST in a loop, so that in case it occasionally crashes (DOH!)
# it will continue.
#
while /bin/true
do
	streamTranscoder $*
	sleep 10
done

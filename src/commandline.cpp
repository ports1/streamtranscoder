#include <stdio.h>
#ifndef WIN32
#include <unistd.h>
#include <signal.h>
#else
#include "getopt.h"
#endif
#include <stdlib.h>

#include "log.h"
#include "transcurl.h"
#include "liboddcast.h"
#include "transcode.h"

transcodeGlobals	globals;
int			showConsole = 1;
int			createNewConfig = 0;

void signalHandler(int c) {
	        exit(1);
}

void mainStatusCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Main Status : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Main Status : (%s)\n", (char *)pValue);
	}
}
void inputStatusCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Status : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Status : (%s)\n", (char *)pValue);
	}
}
void readBytesCallback(void *pValue) {
	// pValue is a long
	//LogMessage(LOG_INFO, "We just read : (%d) bytes", (long)pValue);
}
void inputServerTypeCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Server Type : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Server Type : (%s)\n", (char *)pValue);
	}
}
void inputStreamTypeCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Stream Type : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Stream Type : (%s)\n", (char *)pValue);
	}
}
void inputStreamURLCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Stream URL : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Stream URL : (%s)\n", (char *)pValue);
	}
}
void inputStreamNameCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Stream Name : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Stream Name : (%s)\n", (char *)pValue);
	}
}
void inputMetadataCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Metadata : (%s)", (char *)pValue);
	setCurrentSongTitle((char *)pValue);
	if (showConsole) {
		printf("Metadata : (%s)\n", (char *)pValue);
	}

}
void inputBitrateCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Input Bitrate : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Input Bitrate : (%s)\n", (char *)pValue);
	}
}
void outputStatusCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Output status : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Output status : (%s)\n", (char *)pValue);
	}
}
void writeBytesCallback(void *pValue) {
	// pValue is a long
	//LogMessage(LOG_INFO, "We just wrote : (%d) bytes", (char *)pValue);
}
void outputServerTypeCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Output Server Type : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Output Server Type : (%s)\n", (char *)pValue);
	}
}
void outputStreamTypeCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Output Stream Type : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Output Stream Type : (%s)\n", (char *)pValue);
	}
}
void outputBitrateCallback(void *pValue) {
	// pValue is a string
	LogMessage(LOG_INFO, "Output Bitrate : (%s)", (char *)pValue);
	if (showConsole) {
		printf("Output Bitrate : (%s)\n", (char *)pValue);
	}
}


void usage() {
	fprintf(stdout, "usage: transcode -c configfile [ -C -e errorlevel -l logfile -n ]\n");
	fprintf(stdout, "where :\n");
	fprintf(stdout, "	-e = error logging level 1=ERROR, 2=INFO, 3=DEBUG\n");
	fprintf(stdout, "	-c = config file\n");
	fprintf(stdout, "	-C = create an example config file\n");
	fprintf(stdout, "	-l = log file\n");
	fprintf(stdout, "	-n = do not show console messages\n");
	fprintf(stdout, "\n");
	fprintf(stdout, "NOTE: to create a config file run : streamTranscoder -C -c myconfig.cfg\n");
	
	exit(1);
}
static int	backgroundProcess = 0;

int transcode_init(int argc, char **argv)
{
	char	*serverURL = NULL;
	char	*configFile = NULL;
	char	*logFile = NULL;
	char	*defaultConfigFile = "transcoder.cfg";
	char	*defaultLogFile = "transcoder.log";
	int 	c = 0;
	int	printConfig = 0;
	
	memset(&globals, '\000', sizeof(globals));

	// Setup Callbacks for all the interaction
	globals.mainStatusCallback = mainStatusCallback;
	globals.inputStatusCallback = inputStatusCallback;
	globals.readBytesCallback = readBytesCallback;
	globals.inputServerTypeCallback = inputServerTypeCallback;
	globals.inputStreamTypeCallback = inputStreamTypeCallback;
	globals.inputStreamURLCallback = inputStreamURLCallback;
	globals.inputStreamNameCallback = inputStreamNameCallback;
	globals.inputMetadataCallback = inputMetadataCallback;
	globals.inputBitrateCallback = inputBitrateCallback;
	globals.outputStatusCallback = outputStatusCallback;
	globals.writeBytesCallback = writeBytesCallback;
	globals.outputServerTypeCallback = outputServerTypeCallback;
	globals.outputStreamTypeCallback = outputStreamTypeCallback;
	globals.outputBitrateCallback = outputBitrateCallback;

	setServerStatusCallback(globals.outputStatusCallback);
	setGeneralStatusCallback(globals.mainStatusCallback);
	setWriteBytesCallback(globals.writeBytesCallback);
	setBitrateCallback(globals.outputBitrateCallback);
	setStreamTypeCallback(globals.outputStreamTypeCallback);
	setServerTypeCallback(globals.outputServerTypeCallback);
	setSourceURLCallback(globals.inputStreamURLCallback);
	setDestURLCallback(globals.outputStreamURLCallback);


#ifndef WIN32
	signal(SIGUSR1, signalHandler);
#endif
	while ((c = getopt(argc, argv, "hl:c:e:dnC")) != -1) {
		switch (c) {
			case 'l':
				logFile = optarg;
				break;
			case 'C':
				createNewConfig = 1;
				break;
			case 'c':
				configFile = optarg;
				break;
			case 'e':
				setErrorType(atoi(optarg));
				break;
			case 'h':
				usage();
				break;
			case 'n':
				showConsole = 0;
				break;
			default:
				usage();
		}
	}
	if (configFile == NULL) {
		printf("Config file required\n");
		usage();
	}
	setConfigFileName(configFile);
	if (logFile == NULL) {
		logFile = defaultLogFile;
	}
	setLogFile(logFile);
	if (createNewConfig) {
		readConfigFile(0);
		printf("config file created\n");
		exit(1);
	}
	else {
		readConfigFile(1);
	}
	if (printConfig) {
		printConfigFileValues();
	}

	
	return 1;
}

main(int argc, char **argv) {
	int processID = 0;
	transcode_init(argc, argv);
	return transcode_main();
}
